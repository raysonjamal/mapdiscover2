# Whats the aim of MAPDISCOVER? 
All of the developers are heavily OpenStreetMap focused users. However, we all miss the lack of usability on OSM. This project aims to improve the usability of OSM from the end user's point of view.

# What is our goal? 
Develop a new OSM-frontend for the OSM-data with location/frequency based search algorithms and easy to use UI.